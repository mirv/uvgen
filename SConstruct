import sys
import glob
import os
import logging
import multiprocessing
import tarfile
from pathlib import Path

logging.basicConfig()
logging.getLogger().setLevel(logging.DEBUG)

# Set -j option to be the cpu count by default.
SetOption("num_jobs", multiprocessing.cpu_count())
logging.info("building with -j" + str(GetOption("num_jobs")))
# Cache the checksum if the file is older than 8 seconds.
SetOption("max_drift", 8)

top_path = Path(Dir('#').path)
bindir = os.path.join(top_path, "bin")

# default to release build
buildmode = ARGUMENTS.get('mode', 'debug')

AddOption("--sanitise",
          dest="sanitise",
          nargs=1,
          type="int",
          action="store",
          metavar="1",
          help="Compile with sanitiser checks enabled (1), or disabled (0)",
          default=0)

env = Environment(ENV = os.environ)


env.Append(CCFLAGS = ['-Wall', '-Wpedantic', '-march=native', '-std=c++20', '-fPIC'])
env.Append(LIBS = ['pthread'])

builddir = "build"
if buildmode == 'debug' :
    logging.info("debug build active")
    env.Append(CCFLAGS = ["-O0",
                          "-ggdb",
                          "-fPIC"])
    env.Append(CPPDEFINES = ['DEBUG'])
    builddir = os.path.join("build", "debug")
else :
    logging.info("release build active")
    env.Append(CCFLAGS = ["-O2",
                          "-fPIC",
                          "-fomit-frame-pointer",
                          "-fno-exceptions" ])
    env.Append(CPPDEFINES = ['RELEASE', 'NDEBUG'])
    builddir = os.path.join("build", "release")

# Why clone? Maybe something else will be added as a side build to uvgen later.
# SConscript won't pollute the source env that way. Probably a silly thought.
tool_env = env.Clone()
tool_vars = {
    "buildmode" : buildmode,
    "builddir" : builddir,
    "env": tool_env,
	"bindir" : bindir
}

SConscript("src/SConscript",
           variant_dir=builddir,
           duplicate=0,
           exports=tool_vars)

tool_env.Alias("tools", ["uvgen"])

# Set default targets to builds.
Default(["tools"])
Clean("tools", [builddir, bindir])
