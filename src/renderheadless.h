/*
* Vulkan Example - Minimal headless rendering example
*
* Copyright (C) 2017 by Sascha Willems - www.saschawillems.de
*
* This code is licensed under the MIT license (MIT) (http://opensource.org/licenses/MIT)
*/


#include <vulkan/vulkan.h>
#include "VulkanTools.h"

#include <filesystem>
#include <vector>

/**
 * @brief Headless render class.
 *
 * This class has been butchered together from the original renderheadless example. It has
 * the nice feature of a single function to setup and render to file, and then automatically
 * clean up after itself. This does make rendering different types somewhat easy: just call
 * with different parameters.
 *
 * It's a bit ugly though. Ideally there would be some separation of setup, render, and
 * teardown. Maybe someday that work might be done, but in the meantime this will just be
 * hacked further to fix little issues. If anyone else starts to find this useful then there
 * might be more motivation to clean this up somewhat, but it's just not worth the effort
 * right this moment.
 */
class mVulkanHeadless
{
public:

	struct Vertex {
		float position[3];
		float uv[3];
		float normal[3];
	};

	enum class RenderType {
		UV_WIREFRAME,
		UV_HEIGHTMAP,
		UV_NORMALMAP,
		VERTEX_HEIGHTMAP
	};

	static std::filesystem::path m_shader_path;

	mVulkanHeadless(const std::vector<Vertex> &vertices,
	                const std::vector<uint32_t> &indices,
	                const std::string &outname,
	                RenderType type = RenderType::UV_WIREFRAME,
	                int width = 2048,
	                int height = 2048);

	~mVulkanHeadless();

private:

	uint32_t getMemoryTypeIndex(uint32_t typeBits, VkMemoryPropertyFlags properties);
	VkResult createBuffer(VkBufferUsageFlags usageFlags,
	                      VkMemoryPropertyFlags memoryPropertyFlags,
	                      VkBuffer *buffer,
	                      VkDeviceMemory *memory,
	                      VkDeviceSize size,
	                      const void *data = nullptr);
	void submitWork(VkCommandBuffer cmdBuffer, VkQueue queue);

	VkInstance instance;
	VkPhysicalDevice physicalDevice;
	VkDevice device;
	uint32_t queueFamilyIndex;
	VkPipelineCache pipelineCache;
	VkQueue queue;
	VkCommandPool commandPool;
	VkCommandBuffer commandBuffer;
	VkDescriptorSetLayout descriptorSetLayout;
	VkPipelineLayout pipelineLayout;
	VkPipeline pipeline;
	std::vector<VkShaderModule> shaderModules;
	VkBuffer vertexBuffer, indexBuffer;
	VkDeviceMemory vertexMemory, indexMemory;

	struct FrameBufferAttachment {
		VkImage image;
		VkDeviceMemory memory;
		VkImageView view;
	};
	VkFramebuffer framebuffer;
	FrameBufferAttachment colorAttachment, depthAttachment;
	VkRenderPass renderPass;

	VkDebugReportCallbackEXT debugReportCallback{};
};
