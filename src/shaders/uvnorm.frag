#version 450

layout (location = 0) in vec3 inPos;
layout (location = 1) in vec3 inNorm;
layout (location = 0) out vec4 outFragColour;

layout(push_constant) uniform PushConsts {
	vec4 limits_min;
	vec4 limits_max;
} pushConsts;

void main()
{
	vec3 result = (inNorm + vec3(1.0, 1.0, 1.0)) * vec3(0.5, 0.5, 0.5);
	outFragColour = vec4(result, 1.0);
}
