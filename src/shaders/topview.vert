#version 450

layout (location = 0) in vec3 inPos;
layout (location = 1) in vec3 inUV;
layout (location = 2) in vec3 inNormal;

layout (location = 0) out vec3 outPos;
layout (location = 1) out vec3 outNorm;

out gl_PerVertex {
	vec4 gl_Position;
};

void main()
{
	outPos = inPos;
	outNorm = inNormal;
	vec4 pos = vec4(inPos.x, inPos.z, 0.0, 1.0);
	pos *= vec4(2.0, 2.0, 1.0, 1.0);
	pos -= vec4(1.0, 1.0, 0.0, 0.0);
	pos *= vec4(1.0, -1.0, 1.0, 1.0);
	gl_Position = pos;
}
