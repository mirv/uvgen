#version 450

layout (location = 0) in vec3 inPos;
layout (location = 1) in vec3 inNorm;
layout (location = 0) out vec4 outFragColour;

layout(push_constant) uniform PushConsts {
	vec4 limits_min;
	vec4 limits_max;
} pushConsts;

void main()
{
	float scale = (inPos.y - pushConsts.limits_min.y) / (pushConsts.limits_max.y - pushConsts.limits_min.y);
	outFragColour = vec4(scale, scale, scale, 1.0);
}
