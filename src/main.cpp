#include "renderheadless.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>

#include <filesystem>
namespace fs = std::filesystem;

#include <vector>

#define STB_IMAGE_IMPLEMENTATION
#define TINYGLTF_IMPLEMENTATION
#include "tiny_gltf.h"

#include "cxxopts.hpp"


static std::vector<mVulkanHeadless::Vertex> m_vertices;
static std::vector<uint32_t> m_indices;   ///< Indices are always 32bit; 16bit indices will be converted.

static tinygltf::TinyGLTF m_gltf_ctx;


void parseMesh(tinygltf::Model &model, size_t index)
{

	///< @todo: support different primitive types.
	switch (model.meshes[index].primitives[0].mode) {
	case 0: // points.
		break;
	case 1: // lines
		break;
	case 2: // line loop
		break;
	case 3: // line strip
		break;
	case 4: // triangles
		break;
	case 5: // triangle strip
		break;
	case 6: // triangle fan
		break;
	default: break; // Unsupported.
	}


	for (const auto &primitive : model.meshes[index].primitives) {
		if (primitive.indices != -1) {
			// Mesh has vertex indexing. Primitive will indicate accessor index used to find it.
			const tinygltf::Accessor &accessor = model.accessors[primitive.indices];
			const tinygltf::BufferView &buffer_view = model.bufferViews[accessor.bufferView];
			const tinygltf::Buffer &buffer = model.buffers[buffer_view.buffer];


			/**
			 * In the case of multiple primitives, indexing is into the vertices of that primitive,
			 * not all vertices overall. So to be less than optimal, combine everything into one
			 * big buffer and increment indexing as appropriate.
			 */
			if (accessor.componentType == 5123) {
				// Overly pedantic worry about data alignment.
				uint16_t *indices = new uint16_t[accessor.count];
				memcpy(indices, &buffer.data.at(0) + buffer_view.byteOffset, sizeof(uint16_t) * accessor.count);
				for (size_t i = 0; i < accessor.count; ++i) {
					m_indices.push_back(static_cast<uint32_t>(indices[i] + m_vertices.size()));
				}
				delete [] indices;
			} else if (accessor.componentType == 5125) {
				// Overly pedantic worry about data alignment.
				uint32_t *indices = new uint32_t[accessor.count];
				memcpy(indices, &buffer.data.at(0) + buffer_view.byteOffset, sizeof(uint32_t) * accessor.count);
				for (size_t i = 0; i < accessor.count; ++i) {
					m_indices.push_back(static_cast<uint32_t>(indices[i] + m_vertices.size()));
				}
				delete [] indices;
			} else {
				std::cout << "GLTF2.0 accessor component type " << accessor.componentType << " not supported." << std::endl;
			}
		}

		size_t vcount = 0;
		float *p_position = nullptr;
		float *p_uv = nullptr;
		float *p_normal = nullptr;

		for (const auto & [attr_name, attr_index] : primitive.attributes) {
			const tinygltf::Accessor &accessor = model.accessors[attr_index];
			const tinygltf::BufferView &buffer_view = model.bufferViews[accessor.bufferView];
			const tinygltf::Buffer &buffer = model.buffers[buffer_view.buffer];
			if (attr_name == "POSITION") {
				p_position = (float *)(&buffer.data.at(0) + buffer_view.byteOffset);
				vcount = accessor.count;
			} else if (attr_name == "NORMAL") {
				p_normal = (float *)(&buffer.data.at(0) + buffer_view.byteOffset);
			} else if (attr_name == "TEXCOORD_0") {
				p_uv = (float *)(&buffer.data.at(0) + buffer_view.byteOffset);
			}
		}

		float *p_p = p_position;
		float *p_u = p_uv;
		float *p_n = p_normal;

		for (size_t j = 0; j < vcount; ++j) {
			mVulkanHeadless::Vertex v = {};
			v.position[0] = *p_p++;
			v.position[1] = *p_p++;
			v.position[2] = *p_p++;
			if (p_uv) {
				v.uv[0] = *p_u++;
				v.uv[1] = *p_u++;
			}
			if (p_normal) {
				v.normal[0] = *p_n++;
				v.normal[1] = *p_n++;
				v.normal[2] = *p_n++;
			}

			m_vertices.push_back(v);
		}
	}
}

/**
 * Centre the mesh around [0,0,0] and scale the x/z plane to fit within limits of [0,1].
 * This is so that heightmap rendering fits within the output window. There's no need to
 * touch the y co-ordinate.
 */
void normaliseMesh()
{
	float limits[6];
	limits[0] = std::numeric_limits<float>::max();
	limits[1] = std::numeric_limits<float>::max();
	limits[2] = std::numeric_limits<float>::max();
	limits[3] = std::numeric_limits<float>::min();
	limits[4] = std::numeric_limits<float>::min();
	limits[5] = std::numeric_limits<float>::min();
	for (const auto & v : m_vertices) {
		if (v.position[0] < limits[0]) limits[0] = v.position[0];
		if (v.position[0] > limits[3]) limits[3] = v.position[0];
		if (v.position[1] < limits[1]) limits[1] = v.position[1];
		if (v.position[1] > limits[4]) limits[4] = v.position[1];
		if (v.position[2] < limits[2]) limits[2] = v.position[2];
		if (v.position[2] > limits[5]) limits[5] = v.position[2];
	}

	//float offset_x = (limits[3] + limits[0]) * 0.5f;
	//float offset_y = (limits[4] + limits[1]) * 0.5f;
	//float offset_z = (limits[5] + limits[2]) * 0.5f;

	float scale_x = (limits[3] - limits[0]);
	float scale_z = (limits[5] - limits[2]);
	float scale = 1.0f / (scale_x > scale_z ? scale_x : scale_z);

	for (auto & v : m_vertices) {
		v.position[0] = (v.position[0] - limits[0]) * scale;
		//v.position[1] -= offset_y;
		v.position[2] = (v.position[2] - limits[2]) * scale;
	}
}

int main(int argc, char **argv)
{
	cxxopts::Options options("uvgen", "Image generation based on mesh UV mapping.");

	options.add_options()
		("h,help", "Print usage")
		("uvmap", "Output uv wireframe filename (*.png format)", cxxopts::value<std::string>())
		("uvnormal", "Output normal map filename (*.png format)", cxxopts::value<std::string>())
		("uvheight", "Output height map filename (*.png format)", cxxopts::value<std::string>())
		("heightmap", "Output standard heightmap filename, top down view with vertex locations (not UV coordinates)", cxxopts::value<std::string>())
		("width", "Image output width", cxxopts::value<int>()->default_value("2048"))
		("height", "Image output height", cxxopts::value<int>()->default_value("2048"))
		("input", "GLTF2 source file", cxxopts::value<std::string>())
		("mesh", "GLTF mesh name to analyse", cxxopts::value<std::string>())
		;

	auto result = options.parse(argc, argv);

	if (result.count("help")) {
		std::cout << options.help() << std::endl;
		exit(0);
	}

	if (result.count("input") == 0) {
		std::cout << options.help() << std::endl;
		exit(0);
	}


	std::filesystem::path base_path;

#ifndef WIN32  // *nix

	std::error_code errc;
	fs::path self_path = fs::read_symlink("/proc/self/exe", errc);
	if (errc) {
		// Failed to read the symlink, or it doesn't exist. Fall back to argv[0] dir path.
		self_path = argv[0];
	}
	base_path = self_path.parent_path();

#else  // WIN32

	char *path = new char[4096];
	GetModuleFileName(GetModuleHandle(NULL), path, 4096);
	char *dirpath = strrchr(path, '\\');
	if(dirpath != NULL)
		*dirpath = 0;
	base_path = path;
	delete [] path;

#endif

	//std::filesystem::current_path(base_path);
	mVulkanHeadless::m_shader_path = base_path / "shaders";

	std::string errs;
	tinygltf::Model model;

	std::string filename = result["input"].as<std::string>();
	if (!m_gltf_ctx.LoadASCIIFromFile(&model, &errs, filename)) {
		std::cerr << "Could not load " << filename << std::endl;
		exit(-1);
	}

	int mesh_index = 0;
	if (result.count("mesh")) {
		std::string name = result["mesh"].as<std::string>();
		mesh_index = -1;
		for (size_t i = 0; i < model.meshes.size(); ++i) {
			if (model.meshes[i].name == name) {
				mesh_index = i;
				break;
			}
		}

		if (mesh_index < 0) {
			std::cerr << "Could not locate mesh " << name << std::endl;
			exit(-1);
		}
	}

	parseMesh(model, mesh_index);

	int width = result["width"].as<int>();
	int height = result["height"].as<int>();

	if (result.count("uvmap")) {
		std::string uvname = result["uvmap"].as<std::string>();
		mVulkanHeadless headless(m_vertices,
		                         m_indices,
		                         uvname,
		                         mVulkanHeadless::RenderType::UV_WIREFRAME,
		                         width,
		                         height);
	}

	if (result.count("uvnormal")) {
		std::string uvname = result["uvnormal"].as<std::string>();
		mVulkanHeadless headless(m_vertices,
		                         m_indices,
		                         uvname,
		                         mVulkanHeadless::RenderType::UV_NORMALMAP,
		                         width,
		                         height);
	}

	if (result.count("uvheight")) {
		std::string uvname = result["uvheight"].as<std::string>();
		mVulkanHeadless headless(m_vertices,
		                         m_indices,
		                         uvname,
		                         mVulkanHeadless::RenderType::UV_HEIGHTMAP,
		                         width,
		                         height);
	}

	if (result.count("heightmap")) {
		std::string hname = result["heightmap"].as<std::string>();
		normaliseMesh();
		mVulkanHeadless headless(m_vertices,
		                         m_indices,
		                         hname,
		                         mVulkanHeadless::RenderType::VERTEX_HEIGHTMAP,
		                         width,
		                         height);
	}


	return 0;
}
