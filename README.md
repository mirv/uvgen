# uvgen

Image wireframe, normal, and height generator based on mesh UV data layout.

This project uses:

cxxopts
(https://github.com/jarro2783/cxxopts)

renderheadless example from Sascha Willems
(https://github.com/SaschaWillems/Vulkan)
(really heavily modified)

tinygltf
(https://github.com/syoyo/tinygltf)
(and necessary components)


All 3rd party code is MIT licensed.
